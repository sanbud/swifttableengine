//
//  InputCellModel.swift
//  SwiftTableSandbox
//
//  Created by Aleksandr Budaiev on 6/20/19.
//  Copyright © 2019 com.dntl.org. All rights reserved.
//

import UIKit
import DntlTableEngine

class InputCellModel: BaseCellModel {
    
    open var text:String?
    
    convenience public init(height:Float,
                            type:String,
                            cellActionDelegate:CellActionDelegate,
                            text:String) {
        
        self.init(height:height,
                  type:type,
                  cellActionDelegate:cellActionDelegate)
        
        self.text = text
        
        self.isActivatable = true
        //self.isEditable = true
        //_enabled = enabled;
        //_capitalizationType = UITextAutocapitalizationTypeSentences;
        //_fieldID = fieldID;
        
        /// Show accessory by default.
        //self.isAccessoryVisible = true
    }
}
