//
//  InputCell.swift
//  SwiftTableSandbox
//
//  Created by Aleksandr Budaiev on 6/20/19.
//  Copyright © 2019 com.dntl.org. All rights reserved.
//

import UIKit
import DntlTableEngine

class InputCell: BaseCell, UITextFieldDelegate {

    @IBOutlet weak var textField: UITextField!
    
    var icm:InputCellModel?

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    override public func updateCellModel(cellModel:BaseCellModel) {
        
        super.updateCellModel(cellModel: cellModel)
        
        self.icm = cellModel as? InputCellModel
        
        self.textField.text = self.icm?.text
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if (self.textField.inputAccessoryView == nil) /*&& self.icm.isAccessoryVisible*/ {
            self.textField.inputAccessoryView = self.standardToolbar()
            return self.isEditable()
        }
        return true
    }
    
//    - (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
//    if (!textField.inputAccessoryView && _icm.isAccessoryVisible)
//    textField.inputAccessoryView = [self standardToolbar];
//
//    return _icm.editable;
//    }
    
    func isEditable() -> Bool {
        
        //return self.isEditable;
        return true
    }
    
    override open func activate() {
        self.textField.becomeFirstResponder()
    }
    
    
    //==============================================================================
    
//    func enabled() -> Bool {
    
        //return self.enabled;
//    }

    
    
    //==============================================================================
    
    
//    func enabled(enabled:Bool) {
        //self.enabled = enabled;
        //return true
//    }
}
        
