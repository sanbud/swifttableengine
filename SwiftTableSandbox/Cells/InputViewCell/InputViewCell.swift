//
//  InputViewCell.swift
//  SwiftTableSandbox
//
//  Created by Aleksandr Budaiev on 7/31/19.
//  Copyright © 2019 com.dntl.org. All rights reserved.
//

import UIKit
import DntlTableEngine

class InputViewCell: BaseCell, UITextViewDelegate {
    
    @IBOutlet weak var textView: UITextView!
    
    var icm:InputViewCellModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    override public func updateCellModel(cellModel:BaseCellModel) {
        
        super.updateCellModel(cellModel: cellModel)
        
        self.icm = cellModel as? InputViewCellModel
        
        self.textView.text = self.icm?.text
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
//    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if (self.textView.inputAccessoryView == nil) /*&& self.icm.isAccessoryVisible*/ {
            self.textView.inputAccessoryView = self.standardToolbar()
            return self.isEditable()
        }
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        //todo
    }
    
    //    - (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    //    if (!textField.inputAccessoryView && _icm.isAccessoryVisible)
    //    textField.inputAccessoryView = [self standardToolbar];
    //
    //    return _icm.editable;
    //    }
    
    func isEditable() -> Bool {
        
        //return self.isEditable;
        return true
    }
    
    override open func activate() {
        self.textView.becomeFirstResponder()
    }

}
