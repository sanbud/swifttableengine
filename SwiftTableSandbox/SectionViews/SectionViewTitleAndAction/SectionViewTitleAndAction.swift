//
//  SectionViewTitleAndAction.swift
//  SwiftTableSandbox
//
//  Created by Aleksandr Budaiev on 6/21/19.
//  Copyright © 2019 com.dntl.org. All rights reserved.
//

import UIKit
import DntlTableEngine

class SectionViewTitleAndAction: BaseSectionView {
    
    //var view:UIView!
    
    @IBOutlet weak var cellTitleLabel: UILabel!
    @IBOutlet weak var actionButton: UIButton!
    
//    var textCellModel:TextCellModel?
    
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)

    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

    }
    
    
    convenience init(title: String,
                     buttonTitle: String,
                     sectionModel: BaseSectionModel,
                     containerDelegate: CellContainerDelegate) {
        
        
        
        
        self.init(frame: CGRect(x: 0, y: 0, width: 100, height: 80))
        
        self.sectionModel = sectionModel
        self.containerDelegate = containerDelegate
        
        if let v:UIView = Bundle.main.loadNibNamed("SectionViewTitleAndAction",
                                                  owner: self,
                                                  options: nil)?.first as? UIView {
            
            self.frame = bounds
            
            addSubview(v)
            
            v.translatesAutoresizingMaskIntoConstraints = false
            
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[childView]|",
                                                          options: [],
                                                          metrics: nil,
                                                          views: ["childView": v]))
            
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[childView]|",
                                                          options: [],
                                                          metrics: nil,
                                                          views: ["childView": v]))
            
            
            
        }
        
        
        cellTitleLabel?.text = title
        actionButton?.setTitle(buttonTitle, for: .normal)
    }
    
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func onAction(_ sender: Any) {
        
//        self.sectionModel?.isCollapsed = !self.sectionModel?.isCollapsed
        if (self.sectionModel!.isCollapsed) {

            self.expand()
//            self.sectionModel!.expand()
            
//            self.containerDelegate?.expandSection(self.sectionModel!)
            
        } else {
            self.collapse()
//            self.sectionModel!.collapse()
            
//            self.containerDelegate?.collapseSection(self.sectionModel!)
        }
        //self.containerDelegate?.prepareDatasource()
//        self.containerDelegate?.reloadSection(self.sectionModel!)// reloadCellForModel:self.sectionModel animation:UITableViewRowAnimationAutomatic];
        
    }
    
    override func decorate() {
        
        
    }
    
}


