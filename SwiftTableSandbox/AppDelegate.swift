//
//  AppDelegate.swift
//  SwiftTableSandbox
//
//  Created by Aleksandr Bydaiev on 6/12/19.
//  Copyright © 2019 com.dntl.org. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
//        #warning("to switch screen - uncomment here")
        
        let menu =
//            ExpandSectionVC()
            TestViewController()
        
        //StandardTableVC()
        
        let navigationController = UINavigationController(rootViewController: menu)
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
        
        return true
    }

}

