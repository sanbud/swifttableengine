//
//  UsualTableVC.swift
//  SwiftTableSandbox
//
//  Created by Aleksandr Bydaiev on 6/12/19.
//  Copyright © 2019 com.dntl.org. All rights reserved.
//

import Foundation
import UIKit


let cellReuseIdentifier = "cellIdentifier"
let activityTag = 123

class StandardTableVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    let contacts:[[String]] = [
        ["Elon Musk",       "+1-201-3141-5926"],
        ["Bill Gates",      "+1-202-5358-9793"],
        ["Tim Cook",        "+1-203-2384-6264"],
        ["Richard Branson", "+1-204-3383-2795"],
        ["Jeff Bezos",      "+1-205-0288-4197"],
        ["Warren Buffet",   "+1-206-1693-9937"],
        ["The Zuck",        "+1-207-5105-8209"],
        ["Carlos Slim",     "+1-208-7494-4592"],
        ["Bill Gates",      "+1-209-3078-1640"],
        ["Larry Page",      "+1-210-6286-2089"],
        ["Harold Finch",    "+1-211-9862-8034"],
        ["Sergey Brin",     "+1-212-8253-4211"],
        ["Jack Ma",         "+1-213-7067-9821"],
        ["Steve Ballmer",   "+1-214-4808-6513"],
        ["Phil Knight",     "+1-215-2823-0664"],
        ["Paul Allen",      "+1-216-7093-8446"],
        ["Woz",             "+1-217-0955-0582"]
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
//        let tableView:UITableView = UITableView
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        
        self.tableView.separatorStyle = UITableViewCell.SeparatorStyle.singleLine
        self.tableView.allowsSelection = true;
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contacts.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier)
        
        if cell == nil {
            
            cell = UITableViewCell(style: .subtitle, reuseIdentifier: cellReuseIdentifier)
        }
        
        //print("\(#function) --- section = \(indexPath.section), row = \(indexPath.row)")
        
        cell!.textLabel?.text       = contacts[indexPath.row][0]
        cell!.detailTextLabel?.text = contacts[indexPath.row][1]
        cell!.imageView?.image = UIImage(named: "cellPlaceholder")
        cell!.imageView?.downloaded(from: "https://lorempixel.com/400/400/people")
        
        return cell!
    }
    
}


fileprivate extension UIImageView {
    
    func downloaded(from url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        
        contentMode = mode
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data) else {
                    return
            }
            DispatchQueue.main.async() {
                self.image = image
                self.viewWithTag(activityTag)?.removeFromSuperview()
            }
            }.resume()
    }
    
    func downloaded(from link: String, contentMode mode: UIView.ContentMode = .scaleAspectFit) {

        // make sure info.plist contains flag true for network
        //        <key>NSAppTransportSecurity</key>
        //        <dict>
        //        <key>NSAllowsArbitraryLoads</key>
        //        <true/>
        //        </dict>
        
        guard let url = URL(string: link) else {
            return
        }
        
        if ((self.viewWithTag(activityTag)) == nil) {
            
            let activity:UIActivityIndicatorView = UIActivityIndicatorView(frame: self.bounds)
            activity.color = .white
            activity.tag = activityTag
            self.addSubview(activity)
            activity.center = CGPoint(x: self.bounds.size.width/2, y: self.bounds.size.height/2)
            activity.startAnimating()
        }
        
        downloaded(from: url, contentMode: mode)
    }
}


extension UIViewController {
    
    func add(_ child: UIViewController) {
        
        addChild(child)
        view.addSubview(child.view)
        child.didMove(toParent: self)
    }
    
    func remove() {
        guard parent != nil else {
            return
        }
        willMove(toParent: nil)
        removeFromParent()
        view.removeFromSuperview()
    }
    
    func configureChildViewController(childController: UIViewController, onView: UIView?) {
        var holderView = self.view
        if let onView = onView {
            holderView = onView
        }
        addChild(childController)
        holderView?.addSubview(childController.view)
        equalViewConstraints(containerView: holderView!, view: childController.view)
        childController.didMove(toParent: self)
    }
    
    
    private func equalViewConstraints(containerView: UIView, view: UIView) {
        view.translatesAutoresizingMaskIntoConstraints = false
        //pin 100 points from the top of the super
        let pinTop =
            NSLayoutConstraint(item: view, attribute: .top, relatedBy: .equal,
                                        toItem: containerView, attribute: .top, multiplier: 1.0, constant: 0)
        let pinBottom =
            NSLayoutConstraint(item: view, attribute: .bottom, relatedBy: .equal,
                                           toItem: containerView, attribute: .bottom, multiplier: 1.0, constant: 0)
        let pinLeft =
            NSLayoutConstraint(item: view, attribute: .left, relatedBy: .equal,
                                         toItem: containerView, attribute: .left, multiplier: 1.0, constant: 0)
        let pinRight =
            NSLayoutConstraint(item: view, attribute: .right, relatedBy: .equal,
                                          toItem: containerView, attribute: .right, multiplier: 1.0, constant: 0)
        
        containerView.addConstraints([pinTop, pinBottom, pinLeft, pinRight])
    }
    
}

