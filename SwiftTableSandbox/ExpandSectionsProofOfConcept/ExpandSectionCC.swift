//
//  ExpandSectionCC.swift
//  SwiftTableSandbox
//
//  Created by Aleksandr Budaiev on 6/21/19.
//  Copyright © 2019 com.dntl.org. All rights reserved.
//

import UIKit
import DntlTableEngine

class ExpandSectionCC: BaseCellContainer {
    
    var paymentSection:BaseSectionModel?
    
    var bodySection:BaseSectionModel?
    
    override init(cellActionDelegate:CellActionDelegate) {
        
        super.init(cellActionDelegate:cellActionDelegate)
        
        
        let qcm:TextCellModel = TextCellModel.init(height: 60, text: "abq abc", type:"TextCell")
        let wcm:TextCellModel = TextCellModel.init(height: 60, text: "abw abc", type:"TextCell")
        let ecm:TextCellModel = TextCellModel.init(height: 60, text: "abe abc", type:"TextCell")
        let rcm:TextCellModel = TextCellModel.init(height: 60, text: "abr abc", type:"TextCell")
        let tcm:TextCellModel = TextCellModel.init(height: 60, text: "abt abc", type:"TextCell")
        
        paymentSection = BaseSectionModel.init(cellModels: [qcm,wcm,ecm,rcm,tcm])
        
        let testSection = SectionViewTitleAndAction(title: "Section 1",
                                                    buttonTitle: "Open",
                                                    sectionModel: paymentSection!,
                                                    containerDelegate: self)
        
        
        paymentSection!.updateHeaderView(headerView: testSection)
        
        //-----------------------------------------------------------------------------
        
        let acm:TextCellModel = TextCellModel.init(height: 60, text: "aba abc", type:"TextCell")
        let scm:TextCellModel = TextCellModel.init(height: 60, text: "abs abc", type:"TextCell")
        let dcm:TextCellModel = TextCellModel.init(height: 60, text: "abd abc", type:"TextCell")
        let fcm:TextCellModel = TextCellModel.init(height: 60, text: "abf abc", type:"TextCell")
        let gcm:TextCellModel = TextCellModel.init(height: 60, text: "abg abc", type:"TextCell")
        
        bodySection = BaseSectionModel.init(cellModels: [acm,scm,dcm,fcm,gcm])
        
        let test2Section = SectionViewTitleAndAction(title: "Section 2",
                                                     buttonTitle: "Open 2",
                                                     sectionModel: bodySection!,
                                                     containerDelegate: self)
        
        
        bodySection!.updateHeaderView(headerView: test2Section)
        
        self.sections = [paymentSection!,bodySection!]
        
        self.prepareDatasource()
        
        //self.cellActionDelegate?.reloadData()
    }

}
