//
//  ExpandSectionVC.swift
//  SwiftTableSandbox
//
//  Created by Aleksandr Budaiev on 6/21/19.
//  Copyright © 2019 com.dntl.org. All rights reserved.
//

import UIKit

class ExpandSectionVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureChildViewController(childController: ExpandSectionTVC(), onView: self.view)
    }
    
}
