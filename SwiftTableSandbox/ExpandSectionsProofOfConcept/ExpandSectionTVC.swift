//
//  ExpandSectionTVC.swift
//  SwiftTableSandbox
//
//  Created by Aleksandr Budaiev on 6/21/19.
//  Copyright © 2019 com.dntl.org. All rights reserved.
//

import UIKit
import DntlTableEngine


public class ExpandSectionTVC: BaseTVC {
    
    init() {
        
        super.init(withStyle: UITableView.Style.plain)
        
        let cc = ExpandSectionCC(cellActionDelegate:self)
        
        self.cellContainerDelegate = cc
        
        self.updateCellModelsContainer(cellContainerDelegate: cc)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        self.tableView.allowsSelection = true;
        self.clearsSelectionOnViewWillAppear = true;
        //self.automaticallyAdjustsScrollViewInsets = true;
    }

}
