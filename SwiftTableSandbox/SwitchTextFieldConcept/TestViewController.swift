//
//  TestViewController.swift
//  ChildBible
//
//  Created by Aleksandr Bydaiev on 3/26/19.
//  Copyright © 2019 ab.name. All rights reserved.
//

import UIKit
//import DntlTableEngine.Swift

class TestViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureChildViewController(childController: TestTVC(), onView: self.view)
    }
    
}
