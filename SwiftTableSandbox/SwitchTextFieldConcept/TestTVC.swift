//
//  TestTVC.swift
//  ChildBible
//
//  Created by Aleksandr Bydaiev on 3/26/19.
//  Copyright © 2019 ab.name. All rights reserved.
//

import UIKit
import DntlTableEngine


public class TestTVC: BaseTVC {
    
    init() {

        super.init(withStyle: UITableView.Style.plain)

        let tc = TestCellContainer.init(cellActionDelegate: self)

        super.updateCellModelsContainer(cellContainerDelegate:tc)
        self.cellContainerDelegate = TestCellContainer.init(cellActionDelegate:self)
        
        let testCont:TestCellContainer = TestCellContainer.init(cellActionDelegate: self as CellActionDelegate)
        
        self.updateCellModelsContainer(cellContainerDelegate: testCont)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        //self.cellModelsContainerDelegate?.prepareDatasource()
        //self.reloadData()
        
        self.tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        self.tableView.allowsSelection = true;
        self.clearsSelectionOnViewWillAppear = true;
        //self.automaticallyAdjustsScrollViewInsets = true;
    }
    

}
