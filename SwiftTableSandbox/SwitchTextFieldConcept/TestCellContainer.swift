//
//  TestCellContainer.swift
//  ChildBible
//
//  Created by Aleksandr Bydaiev on 3/26/19.
//  Copyright © 2019 ab.name. All rights reserved.
//

import UIKit
import DntlTableEngine

class TestCellContainer: BaseCellContainer {
    
    override init(cellActionDelegate:CellActionDelegate) {
        
        super.init(cellActionDelegate:cellActionDelegate)
        
        
        let tcm:TextCellModel = TextCellModel.init(height: 60, text: "Sample text here", type:"TextCell")
        
        let tscmCenter =
            TextSwitchCellModel(height: 180,
                                text: "Centered switch.\nOn the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment.",
                                type: "TextSwitchCell",
                                isCenterOrient: true)
        
        let tscmTop =
            TextSwitchCellModel(height: 200,
                                text: "Not a Centered switch.\nBut I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.",
                                type:"TextSwitchCell",
                                isCenterOrient: false)
        
        let tfm =
            InputCellModel(height: 80,
                           type: "InputCell",
                           cellActionDelegate:cellActionDelegate,
                           text: "test 1")
        
        let wqe =
        InputViewCellModel(height: 80,
                           type: "InputViewCell",
                           cellActionDelegate: cellActionDelegate,
                           text: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).")
        
        let qfm =
            InputCellModel(height: 80,
                           type: "InputCell",
                           cellActionDelegate:cellActionDelegate,
                           text: "test 2")
        
        let wfm =
            InputCellModel(height: 80,
                           type: "InputCell",
                           cellActionDelegate:cellActionDelegate,
                           text: "test 3")
        
        self.sections = [BaseSectionModel(cellModels: [tcm ,tscmCenter, tscmTop, tfm, wqe, qfm , tcm, wfm])]
        
        
        self.prepareDatasource()
        //self.cellActionDelegate?.reloadData()
    }

    //override func prepareDatasource() {
        
      //  super.prepareDatasource()
    //}
}
