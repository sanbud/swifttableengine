#
# Be sure to run `pod lib lint DntlTableEngine.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'DntlTableEngine'
  s.version          = '0.1.0'
  s.summary          = 'UITableViewController Helper. Swift version.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
  UITableViewController helper. Writed on Swift language, based on Obj-C version.
                       DESC
  
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author       = { "Design and Test Lab" => "office@dnt-lab.com ",'Aleksandr Budaiev' => 'budaiev@dnt-lab.com' }
  s.homepage     = "https://www.dnt-lab.com/"
  s.source       = { :git => "https://bitbucket.org/dntl/", :tag => "#{s.version}" }
  s.swift_version = "5.0"
  s.ios.deployment_target = '10.0'
  s.requires_arc = true
  #s.screenshots     = 'http://github.com/Budaiev/DntlTableEngine/blob/master/Example/DntlTableEngine/screenshots/demo.jpg'
  #s.source           = { :git => 'https://github.com/Budaiev/DntlTableEngine.git', :tag => s.version.to_s }
  #s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'
  
  s.source_files = 'Classes/*.swift','Classes/CellWithSwitch/**/*', 'Classes/CellWithText/**/*'
  
  #s.ios.source_files   = 'Classes/*.swift','Classes/CellWithSwitch/**/*', 'Classes/CellWithText/**/*'
  
  #s.resource_bundles = {
  #  'DntlTableEngine' => ['Classes/CellWithText/*.xib','Classes/CellWithSwitch/*.xib']
  #}

#   s.resource_bundles = {
#       'DntlTableEngine' => ['Classes/CellWithText/CellWithText.xib','Classes/CellWithSwitch/CellWithSwitch.xib']
#   }
#    s.resources = "Assets/*.xib"
   s.resource_bundles = {
       'DntlTableEngine' => 'Assets/*.*'
   }


  #s.frameworks = 'UIKit'
  #s.dependency 'AFNetworking', '~> 2.3'
end
