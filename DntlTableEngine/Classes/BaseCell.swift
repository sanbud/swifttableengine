//
//  BaseCell.swift
//  ChildBible
//
//  Created by Aleksandr Bydaiev on 3/24/19.
//  Copyright © 2019 ab.name. All rights reserved.
//

import UIKit

//#define DNTLShortSide MIN(UIScreen.mainScreen.bounds.size.width, UIScreen.mainScreen.bounds.size.height)

open class BaseCell: UITableViewCell {
    
    static let kToolBarHeight:CGFloat  = 44.0
    
    var shortSide:CGFloat {
        get {
            return min(UIScreen.main.bounds.size.width, UIScreen.main.bounds.size.height)
        }
    }
    
    var cellModel:BaseCellModel?
    
    /** Update cell configuration with cell data model.
     @param cellModel The data model of the cell.
     */
    open func updateCellModel(cellModel:BaseCellModel) {
        
        self.cellModel = cellModel
        //self.accessibilityIdentifier = cellModel.accessibilityID;
    }
    
    
    /// The cell height. It may be calculated or constant.
    
    /// Recalculate cell expected cell height. We assume this will be called when the cell already have an actual width.
    var height:CGFloat?
    
    
    //==============================================================================
    
    /// Move focus to the next activatable cell.
    @objc func nextAct(_ sender: UIBarButtonItem) {
    
        self.cellModel?.cellActionDelegate?.nextForCellModel(cellModel: self.cellModel!)
    }
    
    
    //==============================================================================
    
    /// Move focus to the previous activatable cell.
    @objc func prev(_ sender: UIBarButtonItem) {
        
        self.cellModel?.cellActionDelegate?.prevForCellModel(cellModel: self.cellModel!)
    }
    
    
    //==============================================================================
    
    /// Finish input.
//    _ sender: UITapGestureRecognizer
    @objc func done(_ sender: UIBarButtonItem) {
    
        self.cellModel?.cellActionDelegate?.hideKeyboard()
    }
    
    
    //==============================================================================
    
    /// Make the cell active. I.e. set input focus.
    open func activate() {
    
    }
    
    
    //==============================================================================
    
    /// Recalculate cell expected cell height. We assume this will be called when the cell already have an actual width.
    func updateHeight() {
    
//        self.cellModel?.height
    }
    
//    func expand() {
    
//        self.cellModel?.height
//        _height = kCollapsedCellHeight + 40 + 24 + 16;// 270;
//        _isExpanded = YES;
//    }
    
    
    //==============================================================================
    
    
//    func collapse() {
//        _height = kCollapsedCellHeight;
//        _isExpanded = NO;
//    }
    
    
    //==============================================================================
    
    /// Return a standard input toolbar with Prev/Next/Done.
    open func standardToolbar() -> (UIToolbar) {
        
        let toolBar:UIToolbar =
            UIToolbar(frame: CGRect(x: 0, y: 0, width: shortSide, height: BaseCell.kToolBarHeight))
        
        toolBar.barStyle = .default//.blackTranslucent
        //toolBar.isTranslucent = true
        toolBar.backgroundColor = UIColor.white
        toolBar.tintColor = UIColor.blue

        // TODO: test
        //toolBar.sizeToFit()
        
        // TODO: localization
        // Adding Button ToolBar
        let prevButton = UIBarButtonItem(title: "Prev",
                                         style: .plain,
                                         target: self,
                                         action: #selector(self.prev(_ :)))
        let nextButton = UIBarButtonItem(title: "Next",
                                         style: .plain,
                                         target: self,
                                         action: #selector(self.nextAct(_ :)))
        
        let doneButton = UIBarButtonItem(title: "Done",
                                         style: .plain,
                                         target: self,
                                         action: #selector(self.done(_ :)))
        
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace,
                                          target: nil,
                                          action: nil)
        
        toolBar.setItems([prevButton, nextButton, flexibleSpace, doneButton], animated: false)
        
        toolBar.isUserInteractionEnabled = true
        
        
        
        return toolBar;
    }
    
//    let tap = UITapGestureRecognizer(target: self, action:#selector(self.hideKeyboardTapped(_ :)))
//    self.tableView.addGestureRecognizer(tap)
//
//    self.reregisterCellTypes()
//}
//
//@objc func hideKeyboardTapped(_ sender: UITapGestureRecognizer) {
//    self.view.endEditing(true)
//}
    

    override open func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override open func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
}
