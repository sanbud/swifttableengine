//
//  BaseSectionView.swift
//  ChildBible
//
//  Created by Aleksandr Bydaiev on 3/25/19.
//  Copyright © 2019 ab.name. All rights reserved.
//

import UIKit

open class BaseSectionView: UIView {
    
    /// The model of section.
    /// UI may update own model is it collapsed or not.
    open var sectionModel:BaseSectionModel?
    
    /// The delegate for cells container to notify, that the section should be reloaded.
    open var containerDelegate:CellContainerDelegate?
    
    /// Possible usage: Expand/collapse section.
    var uncollapse:UIButton?
    
    /// Section title.
    var title:UILabel?
    
    /// Section title string.
    var titleString:String?
    
    /// Section title string with attributes.
    // TODO: attributes onboard.
    var titleAttrString:NSAttributedString?
    
    /// Current expected width.
    var width:CGFloat?
    
    
    /**
     * Create view for table section header/footer.
     * @param title The section title.
     * @param sectionModel The model of section.
     * @param cellModelsContainerDelegate The delegate to notify about collapse/expand action.
     */
    
    public init(title:String,
                sectionModel:BaseSectionModel,
                containerDelegate:CellContainerDelegate) {
        
        super.init(frame: CGRect(x: 0, y: 0, width: 100, height: 40))
        
        self.titleString = title
        self.sectionModel = sectionModel
        self.containerDelegate = containerDelegate
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //fatalError("init(coder:) has not been implemented")
    }
    
    open func updateWithWidth(_ width:CGFloat) {
        
        self.width = width
    }
    

    
    
    //==============================================================================
    
    
    open func decorate() {
    
    }
    
    
    //==============================================================================
    
    
    public func expand() {
        
        self.sectionModel?.expand()
        self.decorate()
        
        for cm:BaseCellModel in self.sectionModel!.cellModels! {
            cm.updateHeight(80);
        }
        
        
        self.containerDelegate?.expandSection(self.sectionModel!)
        self.containerDelegate?.reloadSection(self.sectionModel!)
    }
    
    
    //==============================================================================
    
    
    public func collapse() {
        
        self.sectionModel?.collapse()
        self.decorate()
        
        for cm:BaseCellModel in self.sectionModel!.cellModels! {
            cm.updateHeight(0);
        }
        
        self.containerDelegate?.collapseSection(self.sectionModel!)
        self.containerDelegate?.reloadSection(self.sectionModel!)
    }

    
    
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
    
}
