//
//  BaseCellModel.swift
//  ChildBible
//
//  Created by Aleksandr Bydaiev on 3/24/19.
//  Copyright © 2019 ab.name. All rights reserved.
//

import Foundation

public typealias DNTLCellActionBlock = () -> Void

open class BaseCellModel: NSObject {
    
    var type:String?
    //open var isExpanded = true
    
    private var height:Float?
    
    var width:Float?
    
    /// out tvc
    public var cellActionDelegate:CellActionDelegate?
    
    /// An action to perform on tap.
    public var action:DNTLCellActionBlock?
    
    public var isActivatable:Bool?
    
    public var fieldID:NSString?
    
    //-----------------------------------------------------------------------------
    
    public init(height:Float, type:String) {
        super.init()
        
        self.height = height
        self.type = type
        self.isActivatable = false
    }
    
    public init(height:Float, type:String, cellActionDelegate:CellActionDelegate) {
        
        super.init()
        
        self.height = height
        self.type = type
        self.isActivatable = false
        self.cellActionDelegate = cellActionDelegate
    }
    
    public init(height:Float, type:String, action:@escaping DNTLCellActionBlock) {
        
        super.init()
        
        self.height = height
        self.type = type
        self.isActivatable = false
        self.action = action
    }
    
    func updateWidth(_ width:Float) {
        self.width = width
    }
    
    func updateHeight(_ height:Float) {
        self.height = height
    }
    
    func readHeight() -> Float {
        return self.height!
    }
    
    
    func updateIsActivatable(_ isActivatable:Bool) {
        self.isActivatable = isActivatable;
    }
    
    func updateAction(_ action:@escaping DNTLCellActionBlock) {
        self.action = action
    }
    
    /** Setting new type.
     * @param newType The new type of cell model.
     */
    func resetType(newType:String) {
        self.type = newType
    }
}
