//
//  BaseTVC.swift
//  ChildBible
//
//  Created by Aleksandr Bydaiev on 3/25/19.
//  Copyright © 2019 ab.name. All rights reserved.
//

import UIKit

open class BaseTVC: UITableViewController {
    
    /// Default cell models container delegate. CellContainerDelegate
    public var cellContainerDelegate:CellContainerDelegate?
    
    var knownCellTypes:Array<String>?
    
    public init(withStyle:UITableView.Style) {
        
        super.init(style: withStyle)
    }
    
    public init(withStyle:UITableView.Style, cellContainerDelegate:CellContainerDelegate) {
        
        super.init(style: withStyle)
        self.cellContainerDelegate = cellContainerDelegate
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    open func updateCellModelsContainer(cellContainerDelegate:CellContainerDelegate) {
        
        self.cellContainerDelegate = cellContainerDelegate
    }
    
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        
        let tap = UITapGestureRecognizer(target: self, action:#selector(self.hideKeyboardTapped(_ :)))
        
        self.tableView.addGestureRecognizer(tap)
        
        self.reregisterCellTypes()
    }
    
    @objc func hideKeyboardTapped(_ sender: UITapGestureRecognizer) {
        
        self.view.endEditing(true)
    }
    
    
    
    private func activateCellAtIndexPath(indexPath:IndexPath?) {
        
        if (indexPath != nil) {
            
            UIView.animate(withDuration: 0.2, animations: {
                
                self.tableView.scrollToRow(at: indexPath!,
                                           at: UITableView.ScrollPosition.top,
                                           animated:false)
                
            }) { (finished) in
                if (finished) {
                    
                    if let visibleCells:[BaseCell] = self.tableView?.visibleCells as? [BaseCell] {
                        
                        for c:BaseCell in visibleCells {
                            
                            if self.tableView.indexPath(for: c) == indexPath {
                                
                                c.activate()
                                break
                            }
                        }
                    }
                }
            }
        }
    }
    
    // MARK: - Table view data source
    
    override open func numberOfSections(in tableView: UITableView) -> Int {
        
        return self.cellContainerDelegate!.numberOfSections()
    }
    
    override open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.cellContainerDelegate!.numberOfCellsInSection(section)
    }
    
    
    override open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cm:BaseCellModel = self.cellContainerDelegate!.cellModelForIndexPath(indexPath)!
        
        let bc = tableView.dequeueReusableCell(withIdentifier: cm.type!) as? BaseCell
        
        assert(bc != nil, "The cell should exists.")
        
        /// Update the cell width to re-calculate height.
        cm.updateWidth(Float(self.tableView.frame.size.width))
        
        bc?.updateCellModel(cellModel:cm)
        
        ///?
        bc?.setNeedsDisplay()
        
        return bc!
    }
    
    
    override open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let cm:BaseCellModel = self.cellContainerDelegate!.cellModelForIndexPath(indexPath)!
        
        cm.updateWidth(Float(tableView.frame.size.width))
        
        return CGFloat(cm.readHeight())
    }
    
    open override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        let sm:BaseSectionModel = (self.cellContainerDelegate?.sectionModelForIndex(section))!
        
        let header = sm.headerTitle;
        
        return header
    }
    
    open override func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        
        let sm:BaseSectionModel = (self.cellContainerDelegate?.sectionModelForIndex(section))!
        
        let footer = sm.footerTitle;
        
        return footer
    }
    
    open override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
     
        let sm:BaseSectionModel = (self.cellContainerDelegate?.sectionModelForIndex(section))!
        
        return CGFloat(sm.sectionHeaderHeight)
    }
    
    
    open override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let sm:BaseSectionModel = (self.cellContainerDelegate?.sectionModelForIndex(section))!
        
        let header:BaseSectionView = sm.sectionHeaderView!;
        
        header.updateWithWidth(tableView.frame.size.width)
        
        return header;
    }
    
    
    //open override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {}
    //open override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {}
    
    
    //==============================================================================
    
    
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
}

extension BaseTVC : CellActionDelegate {
    
    public func tableContentHeightChanged() {
        
    }
    
    public func nextForCellModel(cellModel:BaseCellModel) {
        
        if let nextActive = self.cellContainerDelegate?.indexPathOfNextActivatableCellModel(cellModel) {
            
            self.activateCellAtIndexPath(indexPath: nextActive)
        }
    }
    
    public func prevForCellModel(cellModel:BaseCellModel) {
        
        if let prevActive = self.cellContainerDelegate?.indexPathOfPreviousActivatableCellModel(cellModel) {
            
            self.activateCellAtIndexPath(indexPath: prevActive)
        }
    }
    
    public func reloadCellModel(_ cellModel:BaseCellModel, animationType:UITableView.RowAnimation) {
        
        let cellIndex:IndexPath = (self.cellContainerDelegate?.indexPathForCellModel(cellModel))!
        
        if (self.tableView.indexPathsForVisibleRows?.contains(cellIndex))! {
            
            self.reloadCellsAtIndexPaths(indexPaths: [cellIndex],
                                         animation: animationType)
        }
        
    }
    
    public func reloadCellsAtIndexPaths(indexPaths:[IndexPath], animation:UITableView.RowAnimation) {
        
        assert(Thread.isMainThread,"should be main thread")
        
        self.tableView.reloadRows(at: indexPaths, with: animation)
    }
    
    
    public func reloadCell(cell:UITableViewCell) {
        
        self.cellContainerDelegate!.prepareDatasource()
        
        /// ? ask
        if let ip:IndexPath = self.tableView.indexPath(for: cell) {
            
            self.tableView.beginUpdates()
            
            self.tableView.reloadRows(at:[ip],
                                      with: UITableView.RowAnimation.automatic)
            
            self.tableView.endUpdates()
        }

    }
    
    public func indexPathForCellModel(cellModel:BaseCellModel) -> IndexPath {
        
        return self.cellContainerDelegate!.indexPathForCellModel(cellModel)!
    }
    
    public func reloadSections(sections:IndexSet) {
        
        assert(Thread.isMainThread,"should be main thread")
        _ = self.tableView.visibleCells /// todo: ask Dima ?
        self.tableView.reloadSections(sections, with: UITableView.RowAnimation.automatic)
    }
    
    public func reloadData() {
        assert(Thread.isMainThread,"should be main thread")
        
        self.reregisterCellTypes()
        self.tableView.reloadData()
    }
    
    public func reregisterCellTypes() {
        
        if(self.cellContainerDelegate != nil) {
            
            self.knownCellTypes = self.cellContainerDelegate!.cellTypes();
            
            for cellID:String in self.cellContainerDelegate!.cellTypes()! {
                
                //let podBundle = Bundle(for: self.classForCoder)
                //let podBundle = Bundle(for: NSClassFromString(cellID)!)
                
                self.tableView.register(UINib.init(nibName: cellID,
                                                   bundle:nil ),
                                        forCellReuseIdentifier: cellID)
            }
        }
    }
    
    public func hideKeyboard() {
        
        self.view.endEditing(true)
    }
    
}


