//
//  BaseCellContainer.swift
//  ChildBible
//
//  Created by Aleksandr Bydaiev on 3/24/19.
//  Copyright © 2019 ab.name. All rights reserved.
//

import Foundation

open class BaseCellContainer: NSObject {
    
    //-----------------------------------------------------------------------------
    
    /// The delegate for common cell actions. Conforms to CellActionDelegate protocol (in most cases TVC).
    open var cellActionDelegate:CellActionDelegate?
    
    /// Array with all sections for the table.
    open var sections:[BaseSectionModel]?
    
    /// A flat array of all cells for easy access.
    open var allCellModels:[BaseCellModel]?
    
    /// An array with all cell types in the current table.
    open var allCellTypes:[String]?
    
    /// Test mode flag.
    open var isTestMode:Bool = false
    
    /// Total height of all cells in the table.
    open var totalHeight:Float = 0
    
    //-----------------------------------------------------------------------------
    
    public init(cellActionDelegate:CellActionDelegate) {

        super.init()
        self.cellActionDelegate = cellActionDelegate
    }
    
    public func updateCellActionDelegate(cellActionDelegate:CellActionDelegate) {
        
        self.cellActionDelegate = cellActionDelegate
    }
    
    /// Prepare data for datasource.
//    public func prepareDataSource() {
//
//        let oldHeight:Float = self.totalHeight;
//
//        self.totalHeight = 0;
//
//        let allCellsTypes = NSMutableSet()
//
//        self.allCellModels = Array()
//        var gatherCellModels:[BaseCellModel] = Array()
//
//        for sm:BaseSectionModel in self.sections! {
//
//            self.totalHeight += sm.sectionHeaderHeight
//
//            for cm:BaseCellModel in sm.cellModels! {
//
//                self.totalHeight += cm.height!
//
//                allCellsTypes.add(cm.type as Any)
//
//                gatherCellModels.append(cm)
//            }
//        }
//
//        self.allCellTypes = (allCellsTypes.allObjects as! [String])
//        self.cellActionDelegate!.reregisterCellTypes()
//        self.allCellModels = gatherCellModels
//
//        if oldHeight != self.totalHeight {
//
//            self.cellActionDelegate!.tableContentHeightChanged()
//        }
//    }
    
}



//=============================================================================


extension BaseCellContainer: CellContainerDelegate {
    
    
    /// All used cell types.
    public func cellTypes() -> [String]? {
        
        return self.allCellTypes!
    }
    
    //=============================================================================
    
    /// Number of section models.
    public func numberOfSections() -> Int {
        
        return self.sections!.count;
    }
    
    //=============================================================================
    
    /// Total number of cells in the current table.
    public func totalNumberOfCells() -> Int {
        
        return self.allCellModels!.count
    }
    
    //=============================================================================
    
    /**
     * Nubler of cells in the section.
     * @param section An index of section.
     */
    public func numberOfCellsInSection(_ section:Int) -> Int {
        
        if let sm:BaseSectionModel = self.sections?[section] {
            
            if let count = sm.cellModels?.count {
                
                return count
            }
        }
        
        return 0
    }
    
    //=============================================================================
    
    /// Total expected height of all cells.
    public func totalCellsHeight() -> Float {
        
        return self.totalHeight
    }
    
    //=============================================================================
    
    /**
     * Return cell model for index path.
     @param indexPath The index path of needed cell model.
     */
    
    public func cellModelForIndexPath(_ indexPath:IndexPath) -> BaseCellModel? {
        
        if let sm:BaseSectionModel = self.sections?[indexPath.section] {
            if let cm:BaseCellModel = sm.cellModels?[indexPath.row] {
                
                return cm
            }
        }
        
        return nil
    }

    
    /** The index path of the cell model.
     @param cellModel The cell model.
     */
    public func indexPathForCellModel(_ cellModel:BaseCellModel) -> IndexPath? {
        
        var ip:IndexPath? = nil
        var section:Int = 0
        
        for sm:BaseSectionModel in self.sections! {
        
            var row:Int = 0
            
            for cm:BaseCellModel in sm.cellModels! {
                
                if cm.isEqual(cellModel) {
                    
                    ip = IndexPath(row: row, section: section)
                    
                    return ip!
                }
                
                row += 1
            }
            
            section += 1
        }
        
        return ip
    }
    
    //=============================================================================
    
    /**
     * Return an index of the section model of NSNotFound.
     * @param sectionModel The section model to search.
     */
    public func indexOfSectionModel(_ sectionModel:BaseSectionModel) -> Int {
        
//        var section:Int = 0
//        for sm:BaseSectionModel in self.sections! {
//            if sm.isEqual(sectionModel) {
//                return section
//            }
//            section += 1
//        }
//        return NSNotFound;
        
        if let index = self.sections?.firstIndex(where: {$0 === sectionModel}) {
            
            return index
        } else {
            
            return NSNotFound
        }
    }
    
    //=============================================================================
    
    /** Return Section model object for the section index.
     @param section An index of the section.
     */
    public func sectionModelForIndex(_ section:Int) -> BaseSectionModel? {
        
        if let sm = self.sections?[section] {
            
            return sm
        } else {
            
            return nil
        }
    }
    
    
    

    public func indexPathOfPreviousActivatableCellModel(_ cellModel:BaseCellModel) -> IndexPath? {
        
        
        if let index = self.allCellModels?.firstIndex(where: {$0 === cellModel}) {
            
            if index == 0 {
                
                /// If current cell is first - return nil.
                return nil;
            }
            
            let modelsBefore = self.allCellModels?.dropLast(self.allCellModels!.count - index)
            
            let filteredArray:[BaseCellModel] = modelsBefore!.filter{$0.isActivatable == true}
            
            if let cm:BaseCellModel = filteredArray.last {
                
                return self.indexPathForCellModel(cm)
            } else {
                
                return nil
            }
        } else {

            return nil;
        }
    }
    
    public func indexPathOfNextActivatableCellModel(_ cellModel: BaseCellModel) -> IndexPath? {
        
        if let index = self.allCellModels?.firstIndex(where: {$0 === cellModel}) {
            
            if index+1 == self.allCellModels?.count {
                
                /// If current cell is last - return nil.
                return nil;
            }
            
            let dropIndex = index+1
            
            let modelsAfter = self.allCellModels?.dropFirst(dropIndex)
            
            let filteredArray:[BaseCellModel] = modelsAfter!.filter {
                $0.isActivatable == true
            }
        
            if let cm:BaseCellModel = filteredArray.first {
                
                return self.indexPathForCellModel(cm)
            } else {
                
                return nil
            }
        } else {
            
            return nil;
        }
    }
    
    public func deleteCellModel(_ cellModel:BaseCellModel) {
        
        if let ip = self.indexPathForCellModel(cellModel) {
            
            let section = self.sections![ip.section]
            
            section.deleteCellModel(cellModel)
        }
    }
    
    public func expandSection(_ sectionModel:BaseSectionModel) {
        //self.prepareDatasource()
        self.reloadSection(sectionModel)
    }
    
    
    //==============================================================================
    
    
    public func collapseSection(_ sectionModel:BaseSectionModel) {
        //self.prepareDatasource()
        self.reloadSection(sectionModel)
    }
    
    
    /** Ask cell action delegate to reload section for section model.
     @param sectionModel The model of the section to reload.
     */
    public func reloadSection(_ sectionModel:BaseSectionModel) {
        
        if let index = self.sections?.firstIndex(where: {$0 === sectionModel}) {
            
            self.cellActionDelegate?.reloadSections(sections: IndexSet(arrayLiteral: index))
        }
    }
    
    /// Prepare data for datasource.
    public func prepareDatasource() {
        
        let oldHeight:Float = self.totalHeight;
        
        self.totalHeight = 0;
        
        let allCellsTypes = NSMutableSet()
        
        self.allCellModels = Array()
        var gatherCellModels:[BaseCellModel] = Array()
        
        for sm:BaseSectionModel in self.sections! {
            
            if !sm.isCollapsed {
                self.totalHeight += sm.sectionHeaderHeight
                
                for cm:BaseCellModel in sm.cellModels! {
                    
                    self.totalHeight += cm.readHeight()
                    
                    allCellsTypes.add(cm.type as Any)
                    
                    gatherCellModels.append(cm)
                }
            } else {
                
                self.totalHeight += sm.sectionHeaderHeight
                
                for cm:BaseCellModel in sm.cellModels! {
                    
                    //self.totalHeight += cm.height!
                    
                    allCellsTypes.add(cm.type as Any)
                    
                    gatherCellModels.append(cm)
                }
            }
            
//            self.totalHeight += sm.sectionHeaderHeight
//
//            for cm:BaseCellModel in sm.cellModels! {
//
//                self.totalHeight += cm.height!
//
//                allCellsTypes.add(cm.type as Any)
//
//                gatherCellModels.append(cm)
//            }
        }
        
        self.allCellTypes = (allCellsTypes.allObjects as! [String])
        self.cellActionDelegate!.reregisterCellTypes()
        self.allCellModels = gatherCellModels
        
        if oldHeight != self.totalHeight {
            
            self.cellActionDelegate!.tableContentHeightChanged()
        }
    }
}

