//
//  TextCell.swift
//  ChildBible
//
//  Created by Aleksandr Bydaiev on 3/26/19.
//  Copyright © 2019 ab.name. All rights reserved.
//

import UIKit

public class TextCell: BaseCell {

    @IBOutlet var cellTextLabel: UILabel?
    
    var textCellModel:TextCellModel?
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override public func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override public func updateCellModel(cellModel:BaseCellModel) {
     
        super.updateCellModel(cellModel: cellModel)
        
        self.textCellModel = cellModel as? TextCellModel
        
        self.cellTextLabel?.text = textCellModel?.text
    }
    
}
