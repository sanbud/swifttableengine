//
//  TextCellModel.swift
//  ChildBible
//
//  Created by Aleksandr Bydaiev on 3/26/19.
//  Copyright © 2019 ab.name. All rights reserved.
//

import UIKit

public class TextCellModel: BaseCellModel {

    var text:String?// = ""
    
    convenience public init(height:Float, text:String, type:String) {
        
        
        
        self.init(height: height, type: type)
        
        //let h = self.isExpanded ? height : 0
        
//        if(!self.isExpanded) {
//
//            self.height = 0
//        }
        
        
        self.text = text
    }
    
//    func expand() {
//
//        self.height = 60
//        self.isExpanded = true
//    }
    
    
    //==============================================================================
    
    
//    func collapse() {
//        
//        self.height = 0
//        self.isExpanded = false
//    }
}
