//
//  CellActionProtocol.swift
//  DntlTableEngine
//
//  Created by Aleksandr Budaiev on 6/19/19.
//

/// The protocol to notify action delegate about common cell action (i.e. move input focus to the next cell).
public protocol CellActionDelegate {
    
    
    func prevForCellModel(cellModel:BaseCellModel)
    func nextForCellModel(cellModel:BaseCellModel)
    
    func tableContentHeightChanged()
    
    
    /** Reload cell for model.
     @param cellModel Cell model to determine which cell need to be reloaded.
     @param animationType Reload animation type.
     */
    func reloadCellModel(_ cellModel:BaseCellModel, animationType:UITableView.RowAnimation)
    
    
    /** Notify the cells action delegate, that some cells was changed and need to be reloaded. Only available from the main thread.
     @param indexPaths An array with the changed cells index paths.
     @param animationType Reload animation type.
     */
    func reloadCellsAtIndexPaths(indexPaths:[IndexPath], animation:UITableView.RowAnimation)
    
    /** Reload given cell.
     @param cell The cell to reload.
     */
    func reloadCell(cell:UITableViewCell)
    
    /** Returns an index path of cell model.
     @param cellModel The cell model to calculate an index path.
     */
    func indexPathForCellModel(cellModel:BaseCellModel) -> IndexPath
    
    /** Reload sections with given indexes. Only available from the main thread.
     @param sections A set with section indexex to reload.
     */
    func reloadSections(sections:IndexSet)
    
    /// Reload all data in the table view. Only available from the main thread.
    func reloadData()
    
    /// Register all cell types in the cell container.
    func reregisterCellTypes()
    
    func hideKeyboard()
}
