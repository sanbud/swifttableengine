//
//  TextSwitchCellModel.swift
//  ChildBible
//
//  Created by Aleksandr Bydaiev on 3/26/19.
//  Copyright © 2019 ab.name. All rights reserved.
//

import UIKit

public class TextSwitchCellModel: BaseCellModel {

    var text:String?// = ""
    var isCenterOrient:Bool?
    
    convenience public init(height:Float, text:String, type:String, isCenterOrient:Bool) {
        
        self.init(height: height, type: type)
        
        self.isCenterOrient = isCenterOrient
        self.text = text
    }
}
