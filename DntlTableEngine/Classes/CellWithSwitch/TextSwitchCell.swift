//
//  TextSwitchCell.swift
//  ChildBible
//
//  Created by Aleksandr Bydaiev on 3/26/19.
//  Copyright © 2019 ab.name. All rights reserved.
//

import UIKit

public class TextSwitchCell: BaseCell {
    
//    var centerForTogglerX:NSLayoutConstraint?
//    var centerForTogglerY:NSLayoutConstraint?
    
    
    @IBOutlet var cellTextLabel: UILabel?
    @IBOutlet var toggler: UISwitch?
    
    var textSwitchCellModel:TextSwitchCellModel?

    override public func awakeFromNib() {
        super.awakeFromNib()
        self.cellTextLabel!.numberOfLines = 0;
        self.cellTextLabel!.translatesAutoresizingMaskIntoConstraints = false
        self.toggler!.translatesAutoresizingMaskIntoConstraints = false
    }

    override public func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override public func updateCellModel(cellModel:BaseCellModel) {
        
        super.updateCellModel(cellModel: cellModel)
        
        self.textSwitchCellModel = cellModel as? TextSwitchCellModel
        
        self.cellTextLabel?.text = self.textSwitchCellModel?.text
        
        
        self.setNeedsLayout()
        
        if (self.textSwitchCellModel?.isCenterOrient)! {
            
            self.centerOrientedConstraints()
        
        } else {
            
            self.topOrientedConstraints()
        }
        
        
        self.layoutIfNeeded()
    }
    
    
    
    func centerOrientedConstraints() {
        
        let textY =
            NSLayoutConstraint(item: self.cellTextLabel!,
                               attribute: NSLayoutConstraint.Attribute.centerY,
                               relatedBy: NSLayoutConstraint.Relation.equal,
                               toItem: self.contentView,
                               attribute: NSLayoutConstraint.Attribute.centerY,
                               multiplier: 1,
                               constant: 0)
        
        let togglerY =
            NSLayoutConstraint(item: self.toggler!,
                               attribute: NSLayoutConstraint.Attribute.centerY,
                               relatedBy: NSLayoutConstraint.Relation.equal,
                               toItem: self.contentView,
                               attribute: NSLayoutConstraint.Attribute.centerY,
                               multiplier: 1,
                               constant: 0)
        
        self.contentView.addConstraints(
            [textY ,
             togglerY])
        
    }
    
    func topOrientedConstraints() {
        
        let textTop =
            NSLayoutConstraint(item: self.cellTextLabel!,
                               attribute: NSLayoutConstraint.Attribute.top,
                               relatedBy: NSLayoutConstraint.Relation.equal,
                               toItem: self.contentView,
                               attribute: NSLayoutConstraint.Attribute.top,
                               multiplier: 1,
                               constant: 10)
        
        let togglerTop =
            NSLayoutConstraint(item: self.toggler!,
                               attribute: NSLayoutConstraint.Attribute.top,
                               relatedBy: NSLayoutConstraint.Relation.equal,
                               toItem: self.contentView,
                               attribute: NSLayoutConstraint.Attribute.top,
                               multiplier: 1,
                               constant: 10)
        
        
        
        self.contentView.addConstraints([textTop,togglerTop])
        
        
    }
    
    override public func updateConstraints() {
         super.updateConstraints()
        
        let textLeft =
            NSLayoutConstraint(item: self.cellTextLabel!,
                               attribute: NSLayoutConstraint.Attribute.leading,
                               relatedBy: NSLayoutConstraint.Relation.equal,
                               toItem: self.contentView,
                               attribute: NSLayoutConstraint.Attribute.leading,
                               multiplier: 1,
                               constant: 16)
        
        
//        let textBottom =
//            NSLayoutConstraint(item: self.contentView,
//                               attribute: NSLayoutConstraint.Attribute.bottom,
//                               relatedBy: NSLayoutConstraint.Relation.equal,
//                               toItem: self.cellTextLabel!,
//                               attribute: NSLayoutConstraint.Attribute.bottom,
//                               multiplier: 1,
//                               constant: 10)
        
        let togglerRight =
            NSLayoutConstraint(item: self.contentView,
                               attribute: NSLayoutConstraint.Attribute.trailing,
                               relatedBy: NSLayoutConstraint.Relation.equal,
                               toItem: self.toggler!,
                               attribute: NSLayoutConstraint.Attribute.trailing,
                               multiplier: 1,
                               constant: 16)
        
        let textToToggler =
            NSLayoutConstraint(item: self.toggler!,
                               attribute: NSLayoutConstraint.Attribute.leading,
                               relatedBy: NSLayoutConstraint.Relation.equal,
                               toItem: self.cellTextLabel!,
                               attribute: NSLayoutConstraint.Attribute.trailing,
                               multiplier: 1,
                               constant: 8)
        
        
        
        self.contentView.addConstraints(
            [textLeft,
             textToToggler,
             togglerRight])
        
    }
    
}

extension NSLayoutConstraint {
    func constraintWithMultiplier(_ multiplier: CGFloat) -> NSLayoutConstraint {
        
        return NSLayoutConstraint(item: self.firstItem!,
                                  attribute: self.firstAttribute,
                                  relatedBy: self.relation,
                                  toItem: self.secondItem,
                                  attribute: self.secondAttribute,
                                  multiplier: multiplier,
                                  constant: self.constant)
    }
}
