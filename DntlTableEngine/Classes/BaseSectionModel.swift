//
//  BaseSectionModel.swift
//  ChildBible
//
//  Created by Aleksandr Bydaiev on 3/25/19.
//  Copyright © 2019 ab.name. All rights reserved.
//

import Foundation

/// The model of the table view section. Contains array of cell models in this section.
public class BaseSectionModel: NSObject {
    
    /// An array of cell models.
    /// @property (nonatomic, strong, readonly) NSArray *cellModels; BaseCellModel
    var cellModels:[BaseCellModel]?
    
    /// The height for section header. Dafault height is 0.
    /// @property (nonatomic, readonly) CGFloat sectionHeaderHeight;
    public var sectionHeaderHeight:Float = 0
    
    /// The view for the section header.
    /// @property (nonatomic, readonly, strong) DNTLSectionHeader *sectionHeaderView;
    public var sectionHeaderView:BaseSectionView?
    
    /// The height for section footer. Dafault height is 0.
    /// @property (nonatomic, readonly) CGFloat sectionFooterHeight;
    var sectionFooterHeight:Float = 0
    
    /// The view for the section footer.
    /// @property (nonatomic, readonly, strong) DNTLSectionHeader *sectionFooterView;
    var sectionFooterView:BaseSectionView?
    
    /// Is the section collapsed.
    /// @property (nonatomic, readonly) BOOL isCollapsed;
    public var isCollapsed:Bool = false
    
    /// The header title text.
    /// @property (nonatomic, readonly, strong) NSString *headerTitle;
    var headerTitle:String?
    
    /// The footer title text.
    /// @property (nonatomic, readonly, strong) NSString *footerTitle;
    var footerTitle:String?
    
    
    /** Create section model with cell models array.
     @param cellModels An array with cell data model.
     */
    public init(cellModels:[BaseCellModel]) {
        
        super.init()
        
        self.cellModels = cellModels
    }
    
    
    /** Create section model with cell models array.
     @param cellModels An array with cell data model.
     @param sectionHeaderHeight The height for section header.
     */
    public convenience init(cellModels:Array<BaseCellModel>, headerHeight:Float) {
        
        self.init(cellModels: cellModels)
        
        self.sectionHeaderHeight = headerHeight
    }
    
    /** Add new cell model to the given index.
     @param cellModel A cell model to add.
     @param index An index to add to.
     */
    public func insertCellModel(cellModel:BaseCellModel, atIndex:Int) {
        
        self.cellModels?.insert(cellModel, at: atIndex)
    }

    
    /** Delete cell model from the section.
     @param cellModel A cell model to delete.
     */
    public func deleteCellModel(_ cellModel:BaseCellModel) {
        
        if let index = self.cellModels?.firstIndex(of: cellModel) {
            
            self.cellModels?.remove(at: index)
        }
    }
    
    
    /** Update section header view.
     @param headerView A new header view.
     */
    public func updateHeaderView(headerView:BaseSectionView) {
        
        self.sectionHeaderView = headerView
        self.sectionHeaderHeight = Float(headerView.frame.size.height)
    }
    
    /** Update section header view height.
     @param headerHeight The new height.
     */
    public func updateHeaderHeight(headerHeight:Float) {
        
        self.sectionHeaderHeight = headerHeight
    }
    
    /** Update section header title.
     @param headerTitle The new section header title.
     */
    public func updateHeaderTitle(headerTitle:String) {
        
        self.headerTitle = headerTitle
    }

    
    /** Update section footer view.
     @param footerView A new footer view.
     */
    public func updateFooterView(footerView:BaseSectionView) {
        
        self.sectionFooterView = footerView
        self.sectionFooterHeight = Float(footerView.frame.size.height)
    }
    
    
    /** Update section footer title.
     @param footerTitle The new section footertitle.
     */
    public func updateFooterTitle(footerTitle:String) {
        
        self.footerTitle = footerTitle;
    }
    
    /** Replace all cells with new cells array.
     @param cellsArray An array of new cells.
     */
    public func replaceCellsWithArray(cellsArray:[BaseCellModel]) {
        
        self.cellModels = cellsArray
    }
    
    /// Set the sections isCollapsed flag to YES.
    public func collapse() {
        
        self.isCollapsed = true
    }
    
    
    /// Set the sections isCollapsed flag to NO.
    public func expand() {
        
        self.isCollapsed = false
    }
    
}
