/// The protocol for base cell container.
public protocol CellContainerDelegate {
    
    func cellTypes() -> [String]?
    
    //=============================================================================
    
    /// Number of section models.
    func numberOfSections() -> Int
    
    //=============================================================================
    
    /// Total number of cells in the current table.
    func totalNumberOfCells() -> Int
    
    
    /// Total expected height of all cells.
    func totalCellsHeight() -> Float
    
    /**
     * Nubler of cells in the section.
     * @param section An index of section.
     */
    func numberOfCellsInSection(_ section:Int) -> Int
    
    /** Return cell model for index path.
     @param indexPath The index path of needed cell model.
     */
    func cellModelForIndexPath(_ indexPath:IndexPath) -> BaseCellModel?
    
    
    /** The index path of the cell model.
     @param cellModel The cell model.
     */
    func indexPathForCellModel(_ cellModel:BaseCellModel) -> IndexPath?
    
    
    /// Prepare data for datasource.
    func prepareDatasource()
    
    
    /** The index path of the first cell model with the given accessibilityIdentifier.
     @param accessibilityIdentifier The accessibility identifier.
     */
    //- (NSIndexPath *)indexPathForCellModelWithAccessibilityIdentifier:(NSString *)accessibilityIdentifier;
    
    /** The index path of the next cell, available for activation regard to given cell model. Starts from the beginning if the is no activatable cell after given.
     @param cellModel The cell model to start search next.
     */
    func indexPathOfNextActivatableCellModel(_ cellModel:BaseCellModel) -> IndexPath?
    
    /** The index path of the previous cell, available for activation regard to given cell model. Starts from the end if the is no activatable cell after given.
     @param cellModel The cell model to start search previous.
     */
    func indexPathOfPreviousActivatableCellModel(_ cellModel:BaseCellModel) -> IndexPath?
    
    
    /** Delete cell model.
     @param cellModel The cell model to delete.
     */
    //- (void)deleteCellModel:(DNTLBaseCellModel *)cellModel;
    func deleteCellModel(_ cellModel:BaseCellModel)
    
    /** Return Section model object for the section index.
     @param section An index of the section.
     */
    func sectionModelForIndex(_ section:Int) -> BaseSectionModel?
    
    /** Ask cell action delegate to reload section for section model.
     @param sectionModel The model of the section to reload.
     */
    //- (void)reloadSection:(DNTLBaseSectionModel *)sectionModel;
    func reloadSection(_ sectionModel:BaseSectionModel)
    
    /** Ask cell action delegate to expand section for section model.
     @param sectionModel The model of the section to expand.
     */
    // - (void)expandSection:(DNTLBaseSectionModel *)sectionModel;
    func expandSection(_ sectionModel:BaseSectionModel)
    
    /** Ask cell action delegate to collapse section for section model.
     @param sectionModel The model of the section to collapse.
     */
    // - (void)collapseSection:(DNTLBaseSectionModel *)sectionModel;
    func collapseSection(_ sectionModel:BaseSectionModel)
    
    
    /// Notify the cell model container, that tha table view was loaded and appears.
    //- (void)appears;
    
}
